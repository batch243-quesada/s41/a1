const express = require('express');
const auth = require('../auth');

const router = express.Router();

const courseControllers = require('../controllers/courseControllers');

// routes
router.post("/", auth.verify, courseControllers.addCourse);

router.get("/all-active-courses", courseControllers.getAllActive);

router.get("/all-courses", auth.verify, courseControllers.getAllCourses)

// routes with params
router.get("/:courseId", courseControllers.getCourse);

router.put("/update/:courseId", auth.verify, courseControllers.updateCourse)

router.patch("/update/:courseId/archive", auth.verify, courseControllers.archiveCourse)

module.exports = router;